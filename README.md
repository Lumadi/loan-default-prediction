# Loan Default Prediction

This challenge was designed by Data Science Nigeria specifically for the DSN Bootcamp 2018, which takes place 11-14 October.

SuperLender is a local digital lending company, which prides itself in its effective use of credit risk models to deliver profitable and high-impact loan alternative. 
Its assessment approach is based on two main risk drivers of loan default prediction:. 1) willingness to pay and 2) ability to pay. 